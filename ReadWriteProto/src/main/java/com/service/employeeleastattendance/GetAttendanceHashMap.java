package com.service.employeeleastattendance;

import com.util.protoobjects.AttendanceOuterClass;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;

import java.io.IOException;
import java.util.HashMap;

public class GetAttendanceHashMap {

    public static HashMap<Integer, Integer> getAttendanceHasMap(String Path,
                                                                int attendanceNoOfDays) {
        HashMap<Integer, Integer> attendanceHasMap = new HashMap<>();
        Configuration conf = new Configuration();
        try {
            Path inFile = new Path(Path);
            SequenceFile.Reader reader = null;
            try {
                IntWritable key = new IntWritable();
                ImmutableBytesWritable value = new ImmutableBytesWritable();
                reader = new SequenceFile.Reader(conf, SequenceFile.Reader.file(inFile),
                        SequenceFile.Reader.bufferSize(4096));
                while (reader.next(key, value)) {
                    AttendanceOuterClass.Attendance.Builder e =
                            AttendanceOuterClass.Attendance.newBuilder().mergeFrom(value.get());
                    int employee_id = Integer.parseInt(String.valueOf(e.getEmployeeId()));
                    int noOfDaysPresent = 0;
                    for (int i = 0; i < attendanceNoOfDays; i++) {
                        AttendanceOuterClass.DatePresence.Builder datePresence =
                                e.getDatesPresentBuilder(i);
                        if (datePresence.getIsPresent()) {
                            noOfDaysPresent++;
                        }
                    }
                    attendanceHasMap.put(employee_id, noOfDaysPresent);
                }
            } finally {
                if (reader != null) {
                    reader.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return attendanceHasMap;
    }
}
