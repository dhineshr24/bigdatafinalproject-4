package com.service.loadprototohbase;

import com.util.protoobjects.BuildingOuterClass;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile.Reader;

import java.io.IOException;
import java.util.HashMap;

public class GetBuildingCodeHashMap {

    public static HashMap<String, String> getBuildingCodeAndCafeteriaCode(String Path) {
        HashMap<String, String> buildingCodeAndCafeteriaCode = new HashMap<>();
        Configuration conf = new Configuration();
        try {
            Path inFile = new Path(Path);
            Reader reader = null;
            try {
                IntWritable key = new IntWritable();
                ImmutableBytesWritable value = new ImmutableBytesWritable();
                reader = new Reader(conf, Reader.file(inFile), Reader.bufferSize(4096));
                while (reader.next(key, value)) {
                    BuildingOuterClass.Building.Builder e = BuildingOuterClass.Building.newBuilder().
                            mergeFrom(value.get());
                    int building_code = Integer.parseInt(String.valueOf(e.getBuildingCode()));
                    String cafeteria_code = e.getCafeteriaCode();
                    //System.out.println("building_code : " + building_code + " cafeteria_code "
                    // +cafeteria_code);
                    buildingCodeAndCafeteriaCode.put(String.valueOf(building_code), cafeteria_code);
                }

            } finally {
                if (reader != null) {
                    reader.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buildingCodeAndCafeteriaCode;
    }
}
