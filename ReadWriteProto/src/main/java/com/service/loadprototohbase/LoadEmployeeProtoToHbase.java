package com.service.loadprototohbase;

import com.util.CreateHbaseTable;
import com.util.protoobjects.EmployeeOuterClass;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.TableNotFoundException;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2;
import org.apache.hadoop.hbase.mapreduce.LoadIncrementalHFiles;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.ArrayList;

public class LoadEmployeeProtoToHbase extends Configured implements Tool {

    private String employeeProtojobName;
    private String employeeSeqFile;
    private String employeeTableName;
    private String employeeProtoHbasePath;

    public LoadEmployeeProtoToHbase(String employeeProtojobName, String employeeSeqFile,
                                    String employeeTableName, String employeeProtoHbasePath) {
        this.employeeProtojobName = employeeProtojobName;
        this.employeeSeqFile = employeeSeqFile;
        this.employeeTableName = employeeTableName;
        this.employeeProtoHbasePath = employeeProtoHbasePath;
    }

    public static class EmployeeMapper extends Mapper<IntWritable, ImmutableBytesWritable,
            ImmutableBytesWritable, Put> {
        private static final byte[] CF_BYTES1 = Bytes.toBytes("employee_details");
        private static final byte[] QUAL_BYTES1 = Bytes.toBytes("employee_qual");

        @Override
        protected void map(IntWritable key, ImmutableBytesWritable value, Context context)
                throws IOException, InterruptedException {
            if (key.get() == 0 || value.getLength() == 0) {
                return;
            }

            EmployeeOuterClass.Employee.Builder employee =
                    EmployeeOuterClass.Employee.newBuilder().mergeFrom(value.get());
            byte[] rowKey = Bytes.toBytes(String.valueOf(key));
            Put put = new Put(rowKey);

            put.addColumn(CF_BYTES1, QUAL_BYTES1, employee.build().toByteArray());
            context.write(new ImmutableBytesWritable(rowKey), put);
        }

    }

    public int run(final String[] args) {
        boolean success = false;
        Job job = null;

        try {
            job = new Job();
            job.setJobName(employeeProtojobName);
            job.setJarByClass(EmployeeMapper.class);
            job.setMapperClass(EmployeeMapper.class);
            job.setInputFormatClass(SequenceFileInputFormat.class);
            job.setMapOutputKeyClass(ImmutableBytesWritable.class);
            job.setMapOutputValueClass(Put.class);

            Configuration conf = HBaseConfiguration.create(getConf());
            setConf(conf);
            ArrayList<String> employeeColFamList = new ArrayList<>();
            employeeColFamList.add("employee_details");
            String tableNameString = employeeTableName;
            try (Connection connection = ConnectionFactory.createConnection(conf)) {
                new CreateHbaseTable(connection, employeeTableName,
                        employeeColFamList).CreateHbaseTable();
                TableName tableName = TableName.valueOf(tableNameString);
                Table table = connection.getTable(tableName);
                RegionLocator regionLocator = connection.getRegionLocator(tableName);
                HFileOutputFormat2.configureIncrementalLoad(job, table, regionLocator);
            }

            String outPath = employeeProtoHbasePath;
            String input = employeeSeqFile;
            FileInputFormat.setInputPaths(job, new Path(input));
            FileOutputFormat.setOutputPath(job, new Path(outPath));

            success = job.waitForCompletion(true);
            doBulkLoad(tableNameString, new Path(outPath));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return success ? 0 : 1;
    }

    private void doBulkLoad(String tableNameString, Path tmpPath) {
        LoadIncrementalHFiles loader = new LoadIncrementalHFiles(getConf());
        try (Connection connection = ConnectionFactory.createConnection(getConf());
             Admin admin = connection.getAdmin()) {
            TableName tableName = TableName.valueOf(tableNameString);
            Table table = connection.getTable(tableName);
            RegionLocator regionLocator = connection.getRegionLocator(tableName);
            loader.doBulkLoad(tmpPath, admin, table, regionLocator);
        } catch (TableNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}