package com.service.csvtohdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

public class LoadCsvToHdfs {
    public static void LoadToHdfs(Configuration conf, String source, String destination) {
        try {
            FileSystem fs = FileSystem.get(conf);
            fs.copyFromLocalFile(new Path(source),
                    new Path(destination));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
